#!/bin/bash

###
### Requirement: apt install libnss3-tools
###

###
### CA file to install (customize!)
###

certfile="Cio_Sign_Root_CA.crt"
certname="Cio_Sign_Root_CA"

###
### For cert9 (SQL)
###

for certDB in $(find ~/ -writable -name "cert9.db")
do
    certdir=$(dirname ${certDB});
    certutil -D -n "${certname}" -t "TC,C,T" -i ${certfile} -d sql:${certdir}
    echo $certname "Removido de tu navegador disponible"
done
